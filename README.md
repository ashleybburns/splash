# splash

Node.js app using [Express 4](http://expressjs.com/).

## Running Locally

Make sure you have [Node.js](http://nodejs.org/) and the [Heroku Toolbelt](https://toolbelt.heroku.com/) installed.


cd splash
npm install
node app.js
```

Your app should now be running on [localhost:5000](http://localhost:5000/).